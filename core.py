"""Simple shooting game, use mouse to navigate in menu and in upgrades menu
all functions are described and quite easy to understand. In game you need to
survive as long as possible and to bit high score"""

import pygame
import sys
import random
from pygame import mixer

pygame.init()
FpsClock = pygame.time.Clock()
pygame.display.set_caption("Game #3")
pygame.display.set_icon(pygame.image.load('icon.png'))

# fonts and colors
Black = (0, 0, 0)
White = (255, 255, 255)
Red = (255, 0, 0)
Font_title = pygame.font.Font('Intro_Inline.otf', 256)
Font_big = pygame.font.Font('barbie.ttf', 92)
Font_mid = pygame.font.Font('barbie.ttf', 72)
Font_very_big = pygame.font.Font('barbie.ttf', 256)
Font = pygame.font.Font('barbie.ttf', 42)
Font1 = pygame.font.Font('barbie.ttf', 56)
Font_small = pygame.font.Font('barbie.ttf', 32)

Res = (1600, 900)
Window = pygame.display.set_mode(Res)
Win = Window.get_rect()

# loading music
Button_click = mixer.Sound("button_sound.wav")
Gun_sound_lvl1 = mixer.Sound("gun_lvl2.wav")
Gun_sound_lvl2 = mixer.Sound("gun_lvl3.wav")
Gun_sound_lvl3 = mixer.Sound("gun_lvl1.wav")
Gun_sound_lvl4 = mixer.Sound("gun_lvl4.wav")
Bomb_sound = mixer.Sound("bomb_sound.wav")
Explosion_sound = mixer.Sound("explosion.wav")
mixer.music.load("game3_music.mp3")
Start_sound = mixer.Sound("lets go.wav")
Lose_sound = mixer.Sound("you lose.wav")
Lose_live = mixer.Sound("lose_live.wav")

# Loads images
Hearth = pygame.image.load('heart_full.png').convert_alpha()
Hearth_empty = pygame.image.load('heart_empty.png').convert_alpha()
Hearth_add = pygame.image.load('heart_big.png')
Box = pygame.image.load('box_full.png')
Box_empty = pygame.image.load('box_empty.png')
Bomb = pygame.image.load('bomb.png').convert_alpha()
Bomb_empty = pygame.image.load('bomb_empty.png').convert_alpha()
Bomb_plus = pygame.image.load('bomb_plus.png').convert_alpha()
Explosion = pygame.image.load('explosion.png').convert_alpha()
Pause_button = pygame.image.load('pause.png').convert_alpha()
Play_button = pygame.image.load('play_button.png').convert_alpha()
Play_button_clicked = pygame.image.load('play_button_clicked.png').convert()
Background_image = pygame.image.load('background.jpg').convert()
Background_image2 = pygame.image.load('background11.png').convert()
Background_image3 = pygame.image.load('space.png').convert()

# ship
Ship_image = pygame.image.load('ship.png').convert_alpha()
Ship_image_rect = Ship_image.get_rect()
Ship_image_rect.center = Win.center
ship_x = Res[0] / 2 - 150
ship_y = Res[1] / 2 - 150
Shipsize = 100
is_fire = False
new_ship_image = Ship_image

# enemy settings
lvl_1_enemy_number = 4
enemy_velocity_step = 2
lvl_2_enemy_number = 3
enemy2_velocity_step = 2
lvl_4_enemy_number = 2
enemy4_velocity_step = 3
enemy_size = 40
enemy3_velocity_step = 0.6

# enemy1
Enemy_image = []
enemy_x = []
enemy_y = []
enemy_health = []

for i in range(lvl_1_enemy_number):
    Enemy_image.append(pygame.image.load('enemy_1.png').convert_alpha())
    enemy_x.append(random.randint(-700, 2200))
    enemy_y.append(random.randint(-1000, -200))
    enemy_health.append(3)

# enemy2
Enemy2_image = []
enemy2_x = []
enemy2_y = []
enemy2_health = []

for i in range(lvl_1_enemy_number):
    Enemy2_image.append(pygame.image.load('enemy_2.png').convert_alpha())
    enemy2_x.append(random.randint(-700, 2200))
    enemy2_y.append(random.randint(1200, 1800))
    enemy2_health.append(6)

# enemy3
Enemy3_image = pygame.image.load('enemy_big.png').convert_alpha()
enemy3_x = random.randint(1700, 1800)
enemy3_y = random.randint(0, 900)
enemy3_health = 100

# enemy4
Enemy4_image = []
enemy4_x = []
enemy4_y = []
enemy4_health = []

for i in range(lvl_4_enemy_number):
    Enemy4_image.append(pygame.image.load('enemy_3.png').convert_alpha())
    enemy4_x.append(random.randint(-700, -300))
    enemy4_y.append(random.randint(0, 900))
    enemy4_health.append(15)

# bullet
Bullet_small = pygame.image.load('bullet1.png').convert_alpha()
Bullet = pygame.image.load('bullet1.png').convert_alpha()
Bullet_big = pygame.image.load('bullet_big.png').convert_alpha()
Bullet_big_rotate = pygame.image.load('bullet_big_r.png').convert_alpha()
bullet_x = 0
bullet_y = 0

# Upgradable stats in game
bomb_counter = 1
reload_time_reducer = 1
Heart = 3
shooting_direction = "top"
fire_lvl = 1
bullet_speed = 20
points_to_add = 0
score = 0
bullet_speed_x = bullet_speed
bullet_speed_y = bullet_speed
fps = 60
ship_speed = 5
ship_speed_shoot_slowdown = 3
slow_ship_loop_counter = 0


def welcome_massage():
    """"function showing welcome massage on screen"""
    Window.blit(Background_image, (0, 0))
    Window.blit(Font_big.render("Welcome in", True, White),
                (50, 650))
    Window.blit(Font_title.render("GAME 3", True, White), (520, 550))
    pygame.display.update()
    pygame.time.delay(2000)
    menu()


def goodbye_screen():
    """"function showing goodbye massage on screen"""
    while True:
        Window.blit(Background_image, (0, 0))
        Window.blit(Font_big.render("Thanks for playing", True, White),
                    (Res[0] / 2 - 300, Res[1] / 2 - 100))
        Window.blit(Font_big.render("See you soon", True, White),
                    (Res[0] / 2 - 220, Res[1] / 2 - 30))
        pygame.display.update()
        pygame.time.delay(2000)
        sys.exit(0)


def menu():
    """menu of a game, here u can read instruction how to play the game,
    see highscores and after clicking play, play the game, also you can
    exit the game"""
    while True:
        mouse_position = pygame.mouse.get_pos()

        if 260 > mouse_position[0] > 80 and \
                400 > mouse_position[1] > 300:
            color_text_1 = (150, 255, 255)
        else:
            color_text_1 = White
        if 520 > mouse_position[0] > 80 and \
                500 > mouse_position[1] > 400:
            color_text_2 = (150, 255, 255)
        else:
            color_text_2 = White
        if 520 > mouse_position[0] > 80 and \
                600 > mouse_position[1] > 500:
            color_text_3 = (150, 255, 255)
        else:
            color_text_3 = White
        if 240 > mouse_position[0] > 80 and \
                700 > mouse_position[1] > 600:
            color_text_4 = (150, 255, 255)
        else:
            color_text_4 = White
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                goodbye_screen()
        click = pygame.mouse.get_pressed()
        if 260 > mouse_position[0] > 80 and \
                400 > mouse_position[1] > 300 and click[0] == 1:
            Button_click.play()
            mixer.music.play(-1)
            mixer.music.set_volume(0.2)
            Start_sound.play()
            pygame.time.wait(200)
            game_loop()
        if 520 > mouse_position[0] > 80 and \
                500 > mouse_position[1] > 400 and click[0] == 1:
            Button_click.play()
            instruction()
        if 520 > mouse_position[0] > 80 and \
                600 > mouse_position[1] > 500 and click[0] == 1:
            Button_click.play()
            best_score()

        if 240 > mouse_position[0] > 80 and \
                700 > mouse_position[1] > 600 and click[0] == 1:
            Button_click.play()
            goodbye_screen()

        Window.blit(Background_image2, (0, 0))
        Window.blit(pygame.font.Font('Intro_Inline.otf', 214)
                    .render("GAME 3", True, White), (40, 60))
        Window.blit(Font_big.render("Play", True, color_text_1), (100, 300))
        Window.blit(Font_big.render("How to play", True, color_text_2),
                    (100, 400))
        Window.blit(Font_big.render("Best Scores", True, color_text_3),
                    (100, 500))
        Window.blit(Font_big.render("Exit", True, color_text_4), (100, 600))
        pygame.display.update()


def restart_game():
    """function reset all values to it's original values to restart game"""
    global bomb_counter, reload_time_reducer, Heart, shooting_direction, \
        fire_lvl, bullet_speed, points_to_add, score, bullet_speed_x, \
        bullet_speed_y, fps, enemy3_x, enemy3_y, ship_speed, \
        ship_speed_shoot_slowdown, slow_ship_loop_counter, ship_x, ship_y,\
        new_ship_image
    new_ship_image = Ship_image
    ship_x = Res[0] / 2 - 150
    ship_y = Res[1] / 2 - 150
    bomb_counter = 1
    reload_time_reducer = 1
    Heart = 3
    shooting_direction = "top"
    fire_lvl = 1
    bullet_speed = 20
    points_to_add = 0
    score = 0
    bullet_speed_x = bullet_speed
    bullet_speed_y = bullet_speed
    fps = 60
    ship_speed = 5
    ship_speed_shoot_slowdown = 3
    slow_ship_loop_counter = 0

    for list_counter in range(lvl_1_enemy_number):
        enemy_y[list_counter] = random.randint(-800, -200)
        enemy_x[list_counter] = random.randint(-700, 2200)
    for list_counter in range(lvl_2_enemy_number):
        enemy2_x[list_counter] = random.randint(-700, 2200)
        enemy2_y[list_counter] = random.randint(1200, 1800)
    for list_counter in range(lvl_4_enemy_number):
        enemy4_x[list_counter] = random.randint(-700, -300)
        enemy4_y[list_counter] = random.randint(0, 900)

    enemy3_x = random.randint(1700, 1800)
    enemy3_y = random.randint(0, 900)


def instruction():
    """function showing instructions how to play game"""
    while True:
        mouse_position = pygame.mouse.get_pos()

        if 260 > mouse_position[0] > 80 and \
                800 > mouse_position[1] > 700:
            color_text = (150, 255, 255)
        else:
            color_text = White
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                goodbye_screen()
        click = pygame.mouse.get_pressed()
        if 260 > mouse_position[0] > 80 and \
                800 > mouse_position[1] > 700 and click[0] == 1:
            Button_click.play()
            menu()

        Window.blit(Background_image2, (0, 0))
        Window.blit(Font.render("To navigate ship use WSAD or ARROWS",
                                True, White), (100, 200))
        Window.blit(Font.render("SPACE to shot, and SHIFT to bomb",
                                True, White), (100, 250))
        Window.blit(Font.render("Survive as long as possible, without"
                                " being hit by enemy", True, White),
                    (100, 300))
        Window.blit(Font.render("Chose upgrades wisely, if something is maxed"
                                " out upgrading it have no result",
                                True, White), (100, 350))
        Window.blit(Font.render("You can upgrade only bomb beyond lvl 3",
                                True, White), (100, 400))
        Window.blit(Font.render("GOOD LUCK !!!", True, White), (100, 450))
        Window.blit(Font.render("tip - use bombs", True, White), (100, 550))
        Window.blit(Font_big.render("Back", True, color_text), (80, 700))
        pygame.display.update()


def best_score():
    """Function is load high scores from file and write them on screen."""
    while True:
        mouse_position = pygame.mouse.get_pos()

        if 260 > mouse_position[0] > 80 and \
                800 > mouse_position[1] > 700:
            color_text = (150, 255, 255)
        else:
            color_text = White
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                goodbye_screen()
        click = pygame.mouse.get_pressed()
        if 260 > mouse_position[0] > 80 and \
                800 > mouse_position[1] > 700 and click[0] == 1:
            Button_click.play()
            menu()

        high_score_file = open("highscore.txt", 'r')
        score_tab = ['0', '0', '0', '0', '0']
        list_position = 0
        for scores in high_score_file:
            scores = scores.strip()
            score_tab[list_position] = scores
            list_position += 1
        high_score_file.close()

        Window.blit(Background_image2, (0, 0))
        Window.blit(Font1.render("1 - " + score_tab[0],
                                 True, White), (100, 180))
        Window.blit(Font1.render("2 - " + score_tab[1],
                                 True, White), (100, 240))
        Window.blit(Font1.render("3 - " + score_tab[2], True, White),
                    (100, 300))
        Window.blit(Font1.render("4 - " + score_tab[3], True, White),
                    (100, 360))
        Window.blit(Font1.render("5 - " + score_tab[4], True, White),
                    (100, 420))
        Window.blit(Font_big.render("Back", True, color_text), (80, 700))
        pygame.display.update()


def high_score_write():
    """Function compering score in game with scores in highscore file
    and if new score is bigger than score in file over writing this
    smaller score in file. If its not last score also over writing all
    scores below it."""
    high_score_file = open("highscore.txt", 'r+')
    score_tab = [str(int(line)) for line in high_score_file.readlines()]
    global score

    for score_number in range(5):
        if (int(score_tab[score_number])) < score:
            score_temporary = score_tab[score_number]
            score_tab[score_number] = score
            score = int(score_temporary)

    high_score_file.seek(0)
    for score_number in range(5):
        high_score_file.write(str(score_tab[score_number]))
        high_score_file.write("\n")

    high_score_file.close()


def pause_screen():
    """This function shows pause screen and let's you choose between:
    continuing the game, exiting it or restarting,
    also u can go to main menu here."""
    while True:
        mouse_position = pygame.mouse.get_pos()
        if 100 + 400 > mouse_position[0] > 100 and \
                600 + 120 > mouse_position[1] > 600:
            menu_color = (150, 255, 255)
        else:
            menu_color = White
        if 600 + 400 > mouse_position[0] > 600 and \
                600 + 120 > mouse_position[1] > 600:
            restart_color = (150, 255, 255)
        else:
            restart_color = White
        if 1100 + 400 > mouse_position[0] > 1100 and \
                600 + 120 > mouse_position[1] > 600:
            exit_color = (150, 255, 255)
        else:
            exit_color = White

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                goodbye_screen()
                pass
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    Button_click.play()
                    game_loop()
        click = pygame.mouse.get_pressed()
        if 100 + 400 > mouse_position[0] > 100 and \
                600 + 120 > mouse_position[1] > 600 and click[0] == 1:
            Button_click.play()
            mixer.music.stop()
            high_score_write()
            restart_game()
            pygame.time.wait(200)
            menu()
        if 600 + 400 > mouse_position[0] > 600 and \
                600 + 120 > mouse_position[1] > 600 and click[0] == 1:
            Button_click.play()
            high_score_write()
            pygame.time.wait(200)
            restart_game()
            game_loop()
        if 1100 + 400 > mouse_position[0] > 1100 and \
                600 + 120 > mouse_position[1] > 600 and click[0] == 1:
            Button_click.play()
            mixer.music.stop()
            goodbye_screen()
        if 1000 > mouse_position[0] > 650 and \
                450 > mouse_position[1] > 150 and click[0] == 1:
            Button_click.play()
            game_loop()

        Window.blit(Play_button, (600, 100))
        Window.blit((Font_big.render("MENU", True, menu_color)), (185, 620))
        Window.blit(Font_big.render("RESTART", True, restart_color),
                    (635, 620))
        Window.blit((Font_big.render("EXIT", True, exit_color)), (1220, 620))
        pygame.display.update()


def key_binds():
    """Function scanning keys pressed during game and calling appropriate
    functions after pressing certain key, also function is slowing
    ship after ship fire and changing bullet direction, to same ass ship
    direction during fire."""
    global ship_x, ship_y, slow_ship_loop_counter, \
        shooting_direction, new_ship_image, bullet_speed_x, \
        bullet_speed_y, score
    if slow_ship_loop_counter > 0:
        ship_speed_fun = ship_speed - int(ship_speed_shoot_slowdown /
                                          reload_time_reducer)
        slow_ship_loop_counter -= 1
    else:
        ship_speed_fun = ship_speed

    mouse_position = pygame.mouse.get_pos()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                Button_click.play()
                pause_screen()
    keys = pygame.key.get_pressed()
    if keys[pygame.K_u]:
        score += 1
    if keys[pygame.K_LSHIFT] or keys[pygame.K_RSHIFT]:
        bomb_explode()
    if keys[pygame.K_w] or keys[pygame.K_UP]:
        shooting_direction = "top"
        ship_y -= ship_speed_fun
        new_ship_image = Ship_image
    if keys[pygame.K_s] or keys[pygame.K_DOWN]:
        shooting_direction = "bottom"
        ship_y += ship_speed_fun
        new_ship_image = pygame.transform.rotate(Ship_image, 180)
    if keys[pygame.K_a] or keys[pygame.K_LEFT]:
        shooting_direction = "left"
        ship_x -= ship_speed_fun
        new_ship_image = pygame.transform.rotate(Ship_image, 90)
    if keys[pygame.K_d] or keys[pygame.K_RIGHT]:
        shooting_direction = "right"
        ship_x += ship_speed_fun
        new_ship_image = pygame.transform.rotate(Ship_image, -90)

    if keys[pygame.K_SPACE] and not is_fire:
        sound_of_ship_fire(fire_lvl)
        if shooting_direction == "top":
            bullet_speed_y = 0
            bullet_speed_x = -bullet_speed
        elif shooting_direction == "bottom":
            bullet_speed_y = 0
            bullet_speed_x = bullet_speed
        elif shooting_direction == "left":
            bullet_speed_y = -bullet_speed
            bullet_speed_x = 0
        elif shooting_direction == "right":
            bullet_speed_y = bullet_speed
            bullet_speed_x = 0
        slow_ship_loop_counter = 50
        ship_fire(ship_x, ship_y)
    click = pygame.mouse.get_pressed()
    if 80 > mouse_position[0] > 10 and \
            150 > mouse_position[1] > 50 and click[0] == 1:
        Button_click.play()
        pause_screen()

    if ship_y < 0:
        ship_y = 0
    if ship_y > 800:
        ship_y = 800
    if ship_x < 0:
        ship_x = 0
    if ship_x + Shipsize > Res[0]:
        ship_x = 1600 - Shipsize
    Window.blit(new_ship_image, (ship_x, ship_y))


def enemy_movement():
    """Function moving enemies of lvl1 to same position as ship is in."""
    for list_counter in range(lvl_1_enemy_number):
        if ship_y > enemy_y[list_counter]:
            enemy_y[list_counter] += enemy_velocity_step
        if ship_y < enemy_y[list_counter]:
            enemy_y[list_counter] -= enemy_velocity_step
        if ship_x > enemy_x[list_counter]:
            enemy_x[list_counter] += enemy_velocity_step
        if ship_x < enemy_x[list_counter]:
            enemy_x[list_counter] -= enemy_velocity_step


def enemy3_movement():
    """Function moving enemies of lvl3 to same position as ship is in."""
    global enemy3_x, enemy3_y
    if ship_y - 40 > enemy3_y:
        enemy3_y += enemy3_velocity_step
    if ship_y - 40 < enemy3_y:
        enemy3_y -= enemy3_velocity_step
    if ship_x - 20 > enemy3_x:
        enemy3_x += enemy3_velocity_step
    if ship_x - 20 < enemy3_x:
        enemy3_x -= enemy3_velocity_step


def enemy2_movement():
    """Function moving enemies of lvl2 to same position as ship is in."""
    for list_counter in range(lvl_1_enemy_number):
        if ship_y > enemy2_y[list_counter]:
            enemy2_y[list_counter] += enemy2_velocity_step
        if ship_y < enemy2_y[list_counter]:
            enemy2_y[list_counter] -= enemy2_velocity_step
        if ship_x > enemy2_x[list_counter]:
            enemy2_x[list_counter] += enemy2_velocity_step
        if ship_x < enemy2_x[list_counter]:
            enemy2_x[list_counter] -= enemy2_velocity_step


def enemy4_movement():
    """Function moving enemies of lvl4 to same position as ship is in."""
    for list_counter in range(lvl_4_enemy_number):
        if ship_y > enemy4_y[list_counter]:
            enemy4_y[list_counter] += enemy4_velocity_step
        if ship_y < enemy4_y[list_counter]:
            enemy4_y[list_counter] -= enemy4_velocity_step
        if ship_x > enemy4_x[list_counter]:
            enemy4_x[list_counter] += enemy4_velocity_step
        if ship_x < enemy4_x[list_counter]:
            enemy4_x[list_counter] -= enemy4_velocity_step


def bomb_explode():
    """Function which adds bomb functionality, it,s moving all enemies on
    in random places out off screens and adds 5 point to score."""
    global bomb_counter, points_to_add, enemy3_y, enemy3_x
    if bomb_counter > 0:
        Bomb_sound.play()
        for list_counter in range(lvl_1_enemy_number):
            enemy_y[list_counter] = random.randint(-800, -200)
            enemy_x[list_counter] = random.randint(-700, 2200)
        for list_counter in range(lvl_2_enemy_number):
            enemy2_x[list_counter] = random.randint(-700, 2200)
            enemy2_y[list_counter] = random.randint(1200, 1800)
        for list_counter in range(lvl_4_enemy_number):
            enemy4_x[list_counter] = random.randint(-700, -300)
            enemy4_y[list_counter] = random.randint(0, 900)

        enemy3_x = random.randint(1700, 1800)
        enemy3_y = random.randint(0, 900)
        points_to_add = 5
        bomb_counter -= 1
        Window.blit(Explosion, (ship_x - 380, ship_y - 350))
        Window.blit(new_ship_image, (ship_x, ship_y))
        pygame.display.update()
        pygame.time.wait(400)
    else:
        msg = Font_mid.render("No bomb left", True, Red)
        Window.blit(msg, (700, 400))


def in_game_interface():
    """Function creating in game interface. It showing how many lives and bomb
     you have, also it adds pause button as well as showing your score."""
    if Heart == 2:
        Window.blit(Hearth, (1540, 10))
        Window.blit(Hearth, (1480, 10))
        Window.blit(Hearth_empty, (1420, 10))
    elif Heart == 1:
        Window.blit(Hearth, (1540, 10))
        Window.blit(Hearth_empty, (1480, 10))
        Window.blit(Hearth_empty, (1420, 10))
    else:
        Window.blit(Hearth, (1540, 10))
        Window.blit(Hearth, (1480, 10))
        Window.blit(Hearth, (1420, 10))

    if bomb_counter == 0:
        Window.blit(Bomb_empty, (1540, 70))
        Window.blit(Bomb_empty, (1480, 70))
        Window.blit(Bomb_empty, (1420, 70))
    elif bomb_counter == 2:
        Window.blit(Bomb, (1540, 70))
        Window.blit(Bomb, (1480, 70))
        Window.blit(Bomb_empty, (1420, 70))
    elif bomb_counter == 1:
        Window.blit(Bomb, (1540, 70))
        Window.blit(Bomb_empty, (1480, 70))
        Window.blit(Bomb_empty, (1420, 70))
    elif bomb_counter == 3:
        Window.blit(Bomb, (1540, 70))
        Window.blit(Bomb, (1480, 70))
        Window.blit(Bomb, (1420, 70))
    else:
        Window.blit(Font_small.render(str(bomb_counter - 3) + " plus",
                                      True, White), (1340, 80))
        Window.blit(Bomb, (1540, 70))
        Window.blit(Bomb, (1480, 70))
        Window.blit(Bomb, (1420, 70))

    Window.blit(Font_small.render("Score - " + str(score),
                                  True, White), (10, 10))
    Window.blit(Pause_button, (20, 55))


def failure():
    """This function is showing failure massage and restarting the game
    and play's failure sound."""
    global Heart, enemy3_x, enemy3_y
    # Lose_sound.play()

    # reseting enemy positions
    for list_counter in range(lvl_1_enemy_number):
        enemy_y[list_counter] = random.randint(-800, -200)
        enemy_x[list_counter] = random.randint(-700, 2200)
    for list_counter in range(lvl_2_enemy_number):
        enemy2_x[list_counter] = random.randint(-700, 2200)
        enemy2_y[list_counter] = random.randint(1200, 1800)
    for list_counter in range(lvl_4_enemy_number):
        enemy4_x[list_counter] = random.randint(-700, -300)
        enemy4_y[list_counter] = random.randint(0, 900)

    enemy3_x = random.randint(1700, 1800)
    enemy3_y = random.randint(0, 900)

    if Heart > 1:
        Lose_live.play()
        msg1 = Font_mid.render("1 Life Lost", True, Red)
        msg_box1 = msg1.get_rect()
        msg_box1.center = (800, 500)
        Window.blit(msg1, msg_box1)
        Heart -= 1
        pygame.display.flip()
        pygame.time.wait(500)

    else:
        Lose_sound.play()
        Window.blit(Font_very_big.render("DEFEATED", True, Red), (300, 300))
        Window.blit(Font.render("Your Score - " + str(score), True, White),
                    (700, 600))
        pygame.display.flip()
        high_score_write()
        pygame.time.wait(2000)
        restart_game()
        mixer.music.stop()
        menu()


def upgrade_screen_key_listener():
    """This function is upgrading ship statistics after
    clicking on good icon."""
    global fire_lvl, ship_speed, bullet_speed, score, Heart, \
        reload_time_reducer, bomb_counter, ship_speed_shoot_slowdown
    while True:
        mouse_position = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            click = pygame.mouse.get_pressed()
            if 450 > mouse_position[0] > 150 and \
                    560 > mouse_position[1] > 260 and click[0] == 1:
                if ship_speed < 10:
                    ship_speed += 2
                    ship_speed_shoot_slowdown += 2
                score += 1
                game_loop()

            if 950 > mouse_position[0] > 650 and \
                    560 > mouse_position[1] > 260 and click[0] == 1:
                if bullet_speed < 80:
                    bullet_speed += 20
                score += 1
                game_loop()

            if 1450 > mouse_position[0] > 1150 and \
                    560 > mouse_position[1] > 260 and click[0] == 1:
                if fire_lvl < 4:
                    fire_lvl += 1
                score += 1
                game_loop()

            if 895 > mouse_position[0] > 735 and \
                    690 > mouse_position[1] > 540 and click[0] == 1:
                if Heart < 3:
                    Heart += 1
                score += 1
                game_loop()

            if 450 > mouse_position[0] > 150 and \
                    800 > mouse_position[1] > 540 and click[0] == 1:
                if reload_time_reducer < 2:
                    reload_time_reducer += 0.5
                score += 1
                game_loop()

            if 1395 > mouse_position[0] > 1235 and \
                    690 > mouse_position[1] > 540 and click[0] == 1:
                bomb_counter += 1
                score += 1
                game_loop()

            pygame.display.update()


def upgrade_screen():
    """This function is showing icons of upgrades and boxes below upgrades
    to show how many upgrades of certain type you have."""
    Window.blit(Background_image3, (0, 0))
    upgrade_massage = Font_big.render("Choose upgrade", True, White)
    Window.blit(upgrade_massage, (600, 100))
    upgrade_massage = Font.render("Ship speed", True, White)
    if ship_speed == 5:
        Window.blit(Box_empty, (200, 360))
        Window.blit(Box_empty, (280, 360))
        Window.blit(Box_empty, (360, 360))
    elif ship_speed == 7:
        Window.blit(Box, (200, 360))
        Window.blit(Box_empty, (280, 360))
        Window.blit(Box_empty, (360, 360))
    elif ship_speed == 9:
        Window.blit(Box, (200, 360))
        Window.blit(Box, (280, 360))
        Window.blit(Box_empty, (360, 360))
    else:
        Window.blit(Box, (200, 360))
        Window.blit(Box, (280, 360))
        Window.blit(Box, (360, 360))
    Window.blit(upgrade_massage, (220, 300))
    upgrade_massage = Font.render("Bullet speed", True, White)
    if bullet_speed == 20:
        Window.blit(Box_empty, (700, 360))
        Window.blit(Box_empty, (780, 360))
        Window.blit(Box_empty, (860, 360))
    elif bullet_speed == 40:
        Window.blit(Box, (700, 360))
        Window.blit(Box_empty, (780, 360))
        Window.blit(Box_empty, (860, 360))
    elif bullet_speed == 60:
        Window.blit(Box, (700, 360))
        Window.blit(Box, (780, 360))
        Window.blit(Box_empty, (860, 360))
    else:
        Window.blit(Box, (700, 360))
        Window.blit(Box, (780, 360))
        Window.blit(Box, (860, 360))

    Window.blit(upgrade_massage, (710, 300))
    upgrade_massage = Font.render("Bullet power", True, White)
    if fire_lvl == 1:
        Window.blit(Box_empty, (1200, 360))
        Window.blit(Box_empty, (1280, 360))
        Window.blit(Box_empty, (1360, 360))
    elif fire_lvl == 2:
        Window.blit(Box, (1200, 360))
        Window.blit(Box_empty, (1280, 360))
        Window.blit(Box_empty, (1360, 360))
    elif fire_lvl == 3:
        Window.blit(Box, (1200, 360))
        Window.blit(Box, (1280, 360))
        Window.blit(Box_empty, (1360, 360))
    else:
        Window.blit(Box, (1200, 360))
        Window.blit(Box, (1280, 360))
        Window.blit(Box, (1360, 360))
    Window.blit(Hearth_add, (750, 550))
    Window.blit(Bomb_plus, (1250, 550))
    Window.blit(upgrade_massage, (1205, 300))

    upgrade_massage = Font.render("Reload "
                                  "time", True, White)
    if reload_time_reducer == 1:
        Window.blit(Box_empty, (220, 610))
        Window.blit(Box_empty, (300, 610))
    elif reload_time_reducer == 1.5:
        Window.blit(Box, (220, 610))
        Window.blit(Box_empty, (300, 610))
    else:
        Window.blit(Box, (220, 610))
        Window.blit(Box, (300, 610))

    Window.blit(upgrade_massage, (200, 550))

    pygame.display.update()

    upgrade_screen_key_listener()


def sound_of_ship_fire(lvl):
    """This function is playing sound of fire depending on lvl of ship."""
    if lvl == 1:
        Gun_sound_lvl1.play()
    elif lvl == 2:
        Gun_sound_lvl2.play()
    elif lvl == 3:
        Gun_sound_lvl3.play()
    else:
        Gun_sound_lvl4.play()


def ship_fire(x, y):
    """This function is generating bullet on screen in different places
    depending on ship lvl and ship direction."""
    global is_fire
    is_fire = True
    if fire_lvl == 1:
        if shooting_direction == "top" or shooting_direction == "bottom":
            Window.blit(Bullet_small, (x + 45, y + 40))
        else:
            Window.blit(Bullet_small, (x + 40, y + 45))
    elif fire_lvl == 2:
        if shooting_direction == "top" or shooting_direction == "bottom":
            Window.blit(Bullet, (x + 30, y + 10))
            Window.blit(Bullet, (x + 60, y + 10))
        else:
            Window.blit(Bullet, (x + 40, y + 30))
            Window.blit(Bullet, (x + 40, y + 60))
    elif fire_lvl == 3:
        if shooting_direction == "top":
            Window.blit(Bullet_small, (x + 30, y + 30))
            Window.blit(Bullet_big, (x + 48, y + 10))
            Window.blit(Bullet_small, (x + 65, y + 30))
        elif shooting_direction == "bottom":
            Window.blit(Bullet_small, (x + 30, y + 30))
            Window.blit(Bullet_big, (x + 48, y + 40))
            Window.blit(Bullet_small, (x + 65, y + 30))
        elif shooting_direction == "left":
            Window.blit(Bullet_small, (x + 20, y + 30))
            Window.blit(Bullet_big_rotate, (x + 0, y + 48))
            Window.blit(Bullet_small, (x + 20, y + 65))
        else:
            Window.blit(Bullet_small, (x + 20, y + 30))
            Window.blit(Bullet_big_rotate, (x + 30, y + 48))
            Window.blit(Bullet_small, (x + 20, y + 65))
    else:
        if shooting_direction == "top":
            Window.blit(Bullet, (x + 30, y + 30))
            Window.blit(Bullet_big, (x + 48, y + 10))
            Window.blit(Bullet, (x + 65, y + 30))
        elif shooting_direction == "bottom":
            Window.blit(Bullet, (x + 30, y + 30))
            Window.blit(Bullet_big, (x + 48, y + 40))
            Window.blit(Bullet, (x + 65, y + 30))
        elif shooting_direction == "left":
            Window.blit(Bullet, (x + 20, y + 30))
            Window.blit(Bullet_big_rotate, (x + 0, y + 48))
            Window.blit(Bullet, (x + 20, y + 65))
        else:
            Window.blit(Bullet, (x + 20, y + 30))
            Window.blit(Bullet_big_rotate, (x + 30, y + 48))
            Window.blit(Bullet, (x + 20, y + 65))


def checking_ship_enemyship_collision(x, y):
    """This function is checking if enemy hits ship,
    x, y are parameters of enemy."""
    if x - Shipsize < ship_x < x + Shipsize and y - Shipsize < ship_y \
            < y + Shipsize:
        failure()


def checking_enemys_health():
    """Function checking enemy health and if its below 1 then changing
    enemy position to its original position, it is doing it for all enemies."""
    global score, points_to_add, enemy3_x, enemy3_y, enemy3_health, \
        bomb_counter
    for list_counter in range(lvl_1_enemy_number):
        if enemy_health[list_counter] < 1:
            Explosion_sound.play()
            score += 1
            enemy_y[list_counter] = random.randint(-800, -200)
            enemy_x[list_counter] = random.randint(-700, 2200)
            enemy_health[list_counter] = 3
    for list_counter in range(lvl_2_enemy_number):
        if enemy2_health[list_counter] < 1:
            Explosion_sound.play()
            score += 1
            points_to_add = 1
            enemy2_x[list_counter] = random.randint(-700, 2200)
            enemy2_y[list_counter] = random.randint(1200, 1800)
            enemy2_health[list_counter] = 6
    for list_counter in range(lvl_4_enemy_number):
        if enemy4_health[list_counter] < 1:
            Explosion_sound.play()
            score += 1
            points_to_add = 3
            enemy4_x[list_counter] = random.randint(-3900, -2900)
            enemy4_y[list_counter] = random.randint(-100, 1000)
            enemy4_health[list_counter] = 15
    if enemy3_health < 1:
        Explosion_sound.play()
        score += 1
        bomb_counter += 1
        points_to_add = 10
        enemy3_x = random.randint(1700, 1800)
        enemy3_y = random.randint(0, 900)
        enemy3_health = 100


def bullet_enemy_collision(x, y, enemy_health_in_fun, enemy_size_in_fun):
    """This function is checking if bullets hit enemy, if yes then its lowering
    enemy health by lvl of fire of ship (x, y are parameters of enemy)."""
    global score, is_fire
    if x - enemy_size_in_fun < bullet_x < x + enemy_size_in_fun and \
            y - enemy_size_in_fun < bullet_y < y + enemy_size_in_fun:
        is_fire = False
        enemy_health_in_fun -= fire_lvl
        return enemy_health_in_fun

    else:
        return enemy_health_in_fun


def enemy_1_controller():
    """This function is calling all function needed for enemy 1 lvl with good
     arguments."""
    enemy_movement()
    for list_counter in range(lvl_1_enemy_number):
        checking_ship_enemyship_collision(enemy_x[list_counter],
                                          enemy_y[list_counter])
        enemy_health[list_counter] = bullet_enemy_collision(
            enemy_x[list_counter], enemy_y[list_counter],
            enemy_health[list_counter], enemy_size)
        Window.blit(Enemy_image[list_counter],
                    (enemy_x[list_counter], enemy_y[list_counter]))


def enemy_2_controller():
    """This function is calling all function needed for enemy 2 lvl with good
     arguments."""
    enemy2_movement()
    for list_counter in range(lvl_2_enemy_number):
        checking_ship_enemyship_collision(enemy2_x[list_counter],
                                          enemy2_y[list_counter])
        enemy2_health[list_counter] = bullet_enemy_collision(
            enemy2_x[list_counter], enemy2_y[list_counter],
            enemy2_health[list_counter], enemy_size)
        Window.blit(Enemy2_image[list_counter],
                    (enemy2_x[list_counter], enemy2_y[list_counter]))


def enemy_4_controller():
    """This function is calling all function needed for enemy 4 lvl with good
     arguments."""
    enemy4_movement()
    for list_counter in range(lvl_4_enemy_number):
        checking_ship_enemyship_collision(enemy4_x[list_counter],
                                          enemy4_y[list_counter])
        enemy4_health[list_counter] = bullet_enemy_collision(
            enemy4_x[list_counter], enemy4_y[list_counter],
            enemy4_health[list_counter], enemy_size)
        Window.blit(Enemy4_image[list_counter],
                    (enemy4_x[list_counter], enemy4_y[list_counter]))


def enemy_3_controller():
    """This function is calling all function needed for enemy lvl 3 with good
     arguments."""
    global enemy3_health
    enemy3_movement()
    checking_ship_enemyship_collision(enemy3_x + 30, enemy3_y + 30)
    enemy3_health = bullet_enemy_collision(enemy3_x + 30, enemy3_y + 50,
                                           enemy3_health, 70)
    Window.blit(Enemy3_image, (enemy3_x, enemy3_y))


def upgrade_operator():
    """This function is calling upgrade function when score is = to some
    chosen values."""
    global fps, score, points_to_add
    if points_to_add > 0:
        score += 1
        points_to_add -= 1
    if (score % 20) == 0 and score < 70:
        upgrade_screen()
    elif (score % 50) == 0:
        fps += 10
        upgrade_screen()


def difficulty_increase():
    """This function is calling controllers of enemies accelerating difficulty
    with increase of score."""
    enemy_1_controller()
    if score > 50:
        enemy_2_controller()

    if score > 20:
        enemy_3_controller()

    if score > 100:
        enemy_4_controller()


def fire_operator():
    """Function changing position of bullet and calling function
    which putting bullet on screen when is_fire = True.
    Also is setting fire to false if bullet hits edge of screen."""
    global is_fire, bullet_x, bullet_y
    if is_fire:
        ship_fire(bullet_x, bullet_y)

        bullet_y += bullet_speed_x
        bullet_x += bullet_speed_y

    else:
        bullet_x = ship_x
        bullet_y = ship_y

    if bullet_y < 0 or bullet_y > 900 or bullet_x < 0 or bullet_x > 1600:
        is_fire = False
        bullet_x = ship_x
        bullet_y = ship_y


def game_loop():
    """Main game function responsible for calling other functions used in game,
    in good order in every while loop run."""
    while True:
        Window.blit(Background_image3, (0, 0))
        upgrade_operator()
        key_binds()
        difficulty_increase()
        fire_operator()
        in_game_interface()
        checking_enemys_health()
        pygame.display.update()
        FpsClock.tick(fps)


def main():
    """"Function adding global scope protection."""
    welcome_massage()


# adding global scope protection
if __name__ == "__main__":
    main()
